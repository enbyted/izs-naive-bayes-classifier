export interface WordFrequency {
    [k: string]: number;
}

export interface BayesData {
    spamWords: WordFrequency;
    hamWords: WordFrequency;
    words: WordFrequency;
    spamCount: number;
    hamCount: number;
}

export class BayesClassifier {
    private words: WordFrequency;
    private spamWords: WordFrequency;
    private hamWords: WordFrequency;
    private spamCount: number;
    private hamCount: number;

    public constructor(data?: BayesData) {
        this.words = (data ? data.words : {}) || {};
        this.spamWords = (data ? data.spamWords : {}) || {};
        this.hamWords = (data ? data.hamWords : {}) || {};
        this.spamCount = (data ? data.spamCount : 0) || 0;
        this.hamCount = (data ? data.hamCount : 0) || 0;
    }

    public flatten() {
        return {
            spamWords: this.spamWords,
            hamWords: this.hamWords,
        } as BayesData;
    }

    public learnSpam(text: string) {
        const words = this.splitText(text);
        this.join(this.spamWords, words);
        this.join(this.words, words);
        this.spamCount++;
    }

    public learnHam(text: string) {
        const words = this.splitText(text);
        this.join(this.hamWords, words);
        this.join(this.words, words);
        this.hamCount++;
    }

    public classify(text: string): 'spam' | 'ham' {
        const words = this.splitText(text);

        let logSpamP = Math.log(this.spamCount / (this.spamCount + this.hamCount));
        let logHamP = Math.log(this.hamCount / (this.spamCount + this.hamCount));
        const wordsCount = Object.keys(this.words).length;

        for (const word in words) {
            const spamCount = this.spamWords[word] || 0;
            const hamCount = this.hamWords[word] || 0;
            const totalCount = spamCount + hamCount;
            const wordFrequency = words[word];

            if (totalCount === 0)
                continue;
            
            // Add Laplace smoothing
            const pSpam = (spamCount + 1) / (spamCount + wordsCount);
            const pHam = (hamCount + 1) / (hamCount + wordsCount);
            //console.log("Word:", word, ", pSpam:", pSpam, ", pHam:", pHam);

            logSpamP += wordFrequency * Math.log(pSpam);
            logHamP += wordFrequency * Math.log(pHam);
        }

        if(logSpamP > logHamP)
            return 'spam';
        else
            return 'ham';
    }

    private splitText(text: string): WordFrequency {
        const words = text
            .split(/\s+/)
            .map(w => w.replace(/[^a-zA-Z0-9]/g, '').toLowerCase())
            .filter(w => w.length > 3);
        
        const ret: WordFrequency = {};

        for (const word of words) {
            ret[word] = ret[word] ? ret[word] + 1 : 1;
        }

        return ret;
    }

    private join(original: WordFrequency, addon: WordFrequency) {
        for (const k in addon) {
            original[k] = original[k] ? original[k] + addon[k] : addon[k];
        }
    }
}

export default BayesClassifier;