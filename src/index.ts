import Bayes from './BayesClassifier';
import { readFileSync, readFile } from 'fs';
import { simpleParser } from 'mailparser';

const labels = JSON.parse(readFileSync("labels.json", { encoding: 'utf8' }));
const bayes = new Bayes();

async function loadFile(filename: string) {
    return new Promise<Buffer>((resolve, reject) => {
        readFile(filename, (err, data) => {
            if (err) reject(err);
            else resolve(data);
        })
    });
}

async function parseMail(filename: string) {
    const text = await loadFile(`./emails/${filename}`);
    const ret = await simpleParser(text);
    
    return {
        content: ret.text || '',
        subject: ret.subject || '',
        filename,
    };
}

async function train(mails: { [filename: string]: number }) {
    const emails = await Promise.all(Object.keys(mails).map(filename => parseMail(filename)));
    
    for (const email of emails) {
        console.log('Training', email.filename);
        if (mails[email.filename] === 0) {
            bayes.learnSpam(email.content);
            bayes.learnSpam(email.subject);
        } else {
            bayes.learnHam(email.content);
            bayes.learnHam(email.subject);
        }
    }
}

async function test(mails: { [filename: string]: number }) {
    const emails = await Promise.all(Object.keys(mails).map(filename => parseMail(filename)));
    
    let right = 0;
    let wrong = 0;
    let errors = 0;

    for (const email of emails) {
        const res = bayes.classify(email.content + ' ' + email.subject);
        const ans = (mails[email.filename] === 0) ? 'spam' : 'ham';

        if (res === ans) {
            right++;
            //console.log('Tesing', email.filename, '- right');
        }
        else {
            wrong++;
            console.log('Tesing', email.filename, '- wrong', 
                (email.content === '') ? 'Empty content' : '', 
                (email.subject === '') ? 'Empty subject': '');
        }

        if (email.subject === '' || email.content === '')
            errors++;
    }

    return {
        right,
        wrong,
        errors,
        total: right + wrong,
    };
}

async function run() {
   console.log('Training...');
   await train(labels);
    console.log('Testing...');
    const res = await test(labels);

    console.log(`Checked ${res.total} emails, got ${res.right} right and ${res.wrong} wrong. Encountered ${res.errors} errors`);
}

// bayes.learnSpam('Have a pleasurable stay! Get up to 30% off + Flat 20% Cashback on Oyo Room bookings done via Paytm');
// bayes.learnSpam('Lets Talk Fashion! Get flat 40% Cashback on Backpacks, Watches, Perfumes, Sunglasses & more');
// bayes.learnHam('Opportunity with Product firm for Fullstack | Backend | Frontend- Bangalore');
// bayes.learnHam('Javascript Developer, Fullstack Developer in Bangalore- Urgent Requirement');

// console.log(bayes.classify('Scan Paytm QR Code to Pay & Win 100% Cashback'));
// console.log(bayes.classify('Re: Applying for Fullstack Developer'));

run();